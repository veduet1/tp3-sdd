#ifndef ARBRE_H_INCLUDED
#define ARBRE_H_INCLUDED

#include <stdbool.h>
#include <stdio.h>

struct arbre_element {
    int valeur;
    struct arbre_element *fils_gauche;
    struct arbre_element *fils_droite;    
};

typedef struct arbre_element* arbre_t;

arbre_t arbre_alloue();
void arbre_libere(arbre_t);

int hauteur_arbre(arbre_t);

bool arbre_possede(arbre_t, int);

arbre_t arbre_depuis_fichier(FILE *f);
void arbre_retire(arbre_t *, int);

size_t arbre_hauteur(arbre_t);

#endif