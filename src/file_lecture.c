#include "file_lecture.h"

#define BUF_LECTURE_TAILLE 32

file_lecture_t file_lecture_creer(void)
{
    return (file_lecture_t) { NULL, NULL, 0 };
}

void file_lecture_libere(file_lecture_t * const file)
{
    struct file_lecture_element *cur, *nouveau;

    if (file != NULL)
    {
        cur = file->premier;

        while (cur != NULL)
        {
            nouveau = cur->suivant;

            free(cur);

            cur = nouveau;
        }

        file->premier = NULL;
        file->dernier = NULL;
        file->taille = 0;
    }
}

bool priv_file_lecture_ajoute_element(file_lecture_t * const file, int valeur)
{
    struct file_lecture_element * a_ajouter = NULL;
    
    if (file != NULL)
    {
        a_ajouter = malloc(sizeof(struct file_lecture_element));

        if (a_ajouter != NULL)
        {
            a_ajouter->valeur = valeur;
            a_ajouter->suivant = NULL;

            if (file->dernier != NULL)
                file->dernier->suivant = a_ajouter;

            if (file->premier == NULL)
                file->premier = a_ajouter;

            file->dernier = a_ajouter;
            file->taille ++;
        }
    }
}

bool est_nombre_priv(char e)
{
    return e >= '0' && e <= '9';
}

bool est_espace_blanc_priv(char e)
{
    return e == ' ' || e == '\t' || e == '\n' || e == '\0';
}

bool file_lecture_depuis_fichier(FILE const * const fichier, file_lecture_t * const recepteur)
{
    bool ret = false;
    char buf[BUF_LECTURE_TAILLE] = { 0 };
    size_t offset = 0;
    size_t taille_lue = 0;
    size_t i = 0;
    

    if (fichier != NULL && recepteur != NULL)
    {
        ret = true;

        while (ret && (taille_lue = fread(buf + offset, sizeof(char), BUF_LECTURE_TAILLE - offset, fichier)) != 0)
        {
            offset = 0;
            
            for (i = 0; ret && i < taille_lue; i++)
            {
                if (est_espace_blanc_priv(buf[i]))
                {
                    if (i > offset)
                    {
                        buf[i] = '\0';
                        ret = priv_file_lecture_ajoute_element(recepteur, atoi(buf + offset));
                    }
                    offset = i+1;
                }
                else if (!est_nombre_priv(buf[i]))
                {
                    /* Le caractêre actuel n’est pas un nombre  */
                    /* Le fichier est donc mal formé            */
                    /* On abandonne donc la lecture             */
                    ret = false;
                }
            }

            for (i = 0; ret && i < taille_lue - offset; i++)
            {
                buf[i] = buf[i + offset];
                offset = taille_lue - offset;
            }
        }
    }

    return (buf[offset] == 0) ? true : ret;
}

int *file_lecture_vers_liste(file_lecture_t const * const file, size_t * const taille)
{
    int *ret = NULL;
    struct file_lecture_element *cur = NULL;
    size_t i = 0;

    if (file != NULL && taille != NULL)
    {
        ret = calloc(sizeof(int), file->taille);

        if (ret != NULL)
        {
            *taille = file->taille;

            cur = file->premier;

            while (cur != NULL)
            {
                ret[i] = cur->valeur;
                cur = cur->suivant;
                i++;
            }
        }
    }

    return ret;
}