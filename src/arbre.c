#include "arbre.h"
#include "file_lecture.h"

#include <stdlib.h>

arbre_t arbre_alloue()
{
    return NULL;
}

void trie_liste(int * const liste, size_t const taille)
{
    size_t max_pos, i, j;
    int tmp;

    for (i = 0; i < taille; i++)
    {
        max_pos = i;

        for (j = i; j < taille; j++)
            if (liste[j] < liste[max_pos])
                max_pos = j;

        tmp = liste[i];
        liste[i] = liste[max_pos];
        liste[max_pos] = tmp;
    }
}

void retire_doublons(int * const liste, size_t * const taille)
{
    size_t i_entree = 1;
    size_t i_sortie = 0;

    if (liste != NULL && taille != NULL)
    {

        while (i_entree < *taille)
        {
            if (liste[i_entree] != liste[i_sortie])
            {
                i_sortie ++;

                liste[i_sortie] = liste[i_entree];
            }

            i_entree ++;
        }

        *taille = i_sortie + 1;
    }
}

void arbre_libere(arbre_t arbre)
{
    if (arbre != NULL)
    {
        arbre_libere(arbre->fils_gauche);
        arbre_libere(arbre->fils_droite);

        free(arbre);
    }
}

int hauteur_arbre(arbre_t arbre)
{
    int hauteur = 0;
    
    if (arbre != NULL)
    {
        int hauteur_gauche = hauteur_arbre(arbre->fils_gauche);
        int hauteur_droite = hauteur_arbre(arbre->fils_droite);

        hauteur ++;
        if (hauteur_gauche > hauteur_droite)
            hauteur += hauteur_gauche;
        else
            hauteur += hauteur_droite;
    }

    return hauteur;
}

bool arbre_possede(arbre_t arbre, int valeur)
{
    arbre_t cur = arbre;
    bool ret = false;

    while (cur != NULL && !ret)
    {
        if (valeur == cur->valeur)
            ret = true;
        else if (valeur < cur->valeur)
            cur = cur->fils_gauche;
        else
            cur = cur->fils_droite;
    }
    
    return ret;
}

struct arbre_element *arbre_depuis_bornes_priv(int *debut, int *fin)
{
    struct arbre_element *element = NULL;    

    if (fin >= debut)
    {
        element = malloc(sizeof(struct arbre_element));

        int * milieu = (fin - debut) / 2 + debut;

        element->valeur = *milieu;

        element->fils_gauche = arbre_depuis_bornes_priv(debut, milieu - 1);
        element->fils_droite = arbre_depuis_bornes_priv(milieu + 1, fin);
    }

    return element;
}

arbre_t arbre_depuis_fichier(FILE *f)
{
    arbre_t ret = arbre_alloue();
    file_lecture_t file = file_lecture_creer();
    int *liste = NULL;
    size_t liste_taille = 0;

    if (f != NULL &&
        file_lecture_depuis_fichier(f, &file) &&
        (liste = file_lecture_vers_liste(&file, &liste_taille)) != NULL)
    {
        // On possède la liste d’entiers liste, on la trie, on retire les
        // doublons puis on construit l’arbre avec

        trie_liste(liste, liste_taille);

        retire_doublons(liste, &liste_taille);

        ret = arbre_depuis_bornes_priv(liste, liste + liste_taille-1);

        free(liste);
    }

    file_lecture_libere(&file);

    return ret;
}

void arbre_retire(arbre_t * arbre, int valeur)
{
    struct arbre_element **vers_valeur = arbre;
    struct arbre_element *a_supprimer;
    struct arbre_element **recepteur_gauche;
    
    while (vers_valeur != NULL &&
           *vers_valeur != NULL &&
           (*vers_valeur)->valeur != valeur)
    {
        if ((*vers_valeur)->valeur > valeur)
        {
            vers_valeur = &(*vers_valeur)->fils_gauche;
        }
        else
        {
            vers_valeur = &(*vers_valeur)->fils_droite;
        }
    }
    
    if (vers_valeur != NULL &&
        *vers_valeur != NULL)
    {
        a_supprimer = *vers_valeur;
        *vers_valeur = a_supprimer->fils_droite;
        
        if (*vers_valeur != NULL)
        {
            recepteur_gauche = &(*vers_valeur)->fils_gauche;

            while (*recepteur_gauche != NULL)
            {
                recepteur_gauche = &(*recepteur_gauche)->fils_gauche;
            }

            *recepteur_gauche = a_supprimer->fils_gauche;
        }

        free(a_supprimer);
    }
}

size_t arbre_hauteur(arbre_t arbre)
{
    size_t ret = 0;
    size_t taille0, taille1;

    if (arbre != NULL)
    {
        taille0 = arbre_hauteur(arbre->fils_gauche);
        taille1 = arbre_hauteur(arbre->fils_droite);

        ret = ((taille0 > taille1)? taille0 : taille1) + 1;
    }

    return ret;
}