#ifndef FILE_LECTURE_H_INCLUDED
#define FILE_LECTURE_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

struct file_lecture_element {
    int valeur;
    struct file_lecture_element * suivant;
};

typedef struct {
    struct file_lecture_element *premier;
    struct file_lecture_element *dernier;
    size_t taille;
} file_lecture_t;

file_lecture_t file_lecture_creer(void);
void file_lecture_libere(file_lecture_t * const);

bool file_lecture_depuis_fichier(FILE const * const, file_lecture_t * const);

int *file_lecture_vers_liste(file_lecture_t const * const, size_t * const);

#endif