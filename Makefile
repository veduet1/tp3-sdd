build/test : build/main.o build/teZZt.o build/file_lecture.o build/arbre.o
	gcc -o build/test build/teZZt.o build/file_lecture.o build/arbre.o build/main.o
	@echo "Lancer le programme avec  ./build/test" 

build/main.o: test/main.c
	gcc -c test/main.c -o build/main.o -g -IteZZt -Isrc

build/teZZt.o : teZZt/teZZt.h teZZt/teZZt.c
	gcc -c teZZt/teZZt.c -o build/teZZt.o -g -IteZZt

build/file_lecture.o : src/file_lecture.h src/file_lecture.c
	gcc -c src/file_lecture.c -o build/file_lecture.o -g -Wall -Wextra -Isrc

build/arbre.o : src/arbre.h src/arbre.c
	gcc -c src/arbre.c -o build/arbre.o -g -Wall -Wextra -Isrc

clean :
	rm -rf build/*
