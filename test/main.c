#include <teZZt.h>
#include <stdio.h>
#include <string.h>

#include <file_lecture.h>
#include <arbre.h>

void trie_liste(int * const, size_t const);
void retire_doublons(int * const, size_t * const);

BEGIN_TEST_GROUP(file_lecture)

TEST(A) {
    FILE * check = fmemopen("1 64 221 23 11 20 19 02 0", 26, "r");
    int elements[] = {1, 64, 221, 23, 11, 20, 19, 2, 0};

    file_lecture_t file = file_lecture_creer();

    REQUIRE(file_lecture_depuis_fichier(check, &file));

    size_t liste_len;
    int * liste = file_lecture_vers_liste(&file, &liste_len);
    file_lecture_libere(&file);

    CHECK(memcmp(liste, elements, liste_len) == 0);

    free(liste);
    fclose(check);
}

TEST(B) {
    int elements[] = {1, 64, 221, 23, 11, 20, 19, 2, 0};
    int elements_triee[] = { 0, 1, 2, 11, 19, 20, 23, 64, 221 };
    size_t taille = 9;

    trie_liste(elements, taille);

    CHECK(memcmp(elements, elements_triee, taille) == 0);
}

TEST(C) {
    int elements[] = { 1, 5, 6, 6, 9, 18, 18, 20 };
    int elements_sans_doublons[] = { 1, 5, 6, 9, 18, 20 };
    size_t taille = 8;

    retire_doublons(elements, &taille);

    CHECK(memcmp(elements, elements_sans_doublons, taille) == 0);
}


TEST(D) {
    int elements[] = { 1, 5, 6, 9, 18, 20 };
    int elements_sans_doublons[] = { 1, 5, 6, 9, 18, 20 };
    size_t taille = 6;

    retire_doublons(elements, &taille);

    CHECK(memcmp(elements, elements_sans_doublons, taille) == 0);
}

TEST(E) {
    FILE * check = fmemopen("1 64 221 23 11 20 19 02 0", 26, "r");

    arbre_t arbre = arbre_depuis_fichier(check);

    REQUIRE(arbre != NULL);

    CHECK(arbre_possede(arbre, 1));
    CHECK(arbre_possede(arbre, 64));
    CHECK(arbre_possede(arbre, 221));
    CHECK(arbre_possede(arbre, 23));
    CHECK(arbre_possede(arbre, 11));
    CHECK(arbre_possede(arbre, 20));
    CHECK(arbre_possede(arbre, 19));
    CHECK(arbre_possede(arbre, 2));
    CHECK(arbre_possede(arbre, 0));

    arbre_libere(arbre);
    fclose(check);
}

TEST(F) {
    FILE * check = fmemopen("1 64 221 23 11 20 19 02 0", 26, "r");

    arbre_t arbre = arbre_depuis_fichier(check);

    REQUIRE(arbre != NULL);

    arbre_retire(&arbre, 19);

    CHECK(arbre_possede(arbre, 1));
    CHECK(arbre_possede(arbre, 64));
    CHECK(arbre_possede(arbre, 221));
    CHECK(arbre_possede(arbre, 23));
    CHECK(arbre_possede(arbre, 11));
    CHECK(arbre_possede(arbre, 20));
    CHECK(!arbre_possede(arbre, 19));
    CHECK(arbre_possede(arbre, 2));
    CHECK(arbre_possede(arbre, 0));

    arbre_libere(arbre);
    fclose(check);
}

TEST(G) {
    FILE * check = fmemopen("1 64 221 23 11 20 19 02 0", 26, "r");

    arbre_t arbre = arbre_depuis_fichier(check);

    REQUIRE(arbre != NULL);

    CHECK(arbre_hauteur(arbre) == 4);

    arbre_libere(arbre);
    fclose(check);
}

END_TEST_GROUP(file_lecture)


int main(void) {
	RUN_TEST_GROUP(file_lecture); 
 	return 0;
}
